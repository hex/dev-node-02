module.exports = function(app) {
  var User = app.models.User;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;
  var Team = app.models.Team;
  var Costumer = app.models.Customer;
  //create the admin role
  Role.create({
    name: 'admin'
  }, function(err, role) {
    if (err) throw err;
    console.log('Created role:', role);
    //make bob an admin
    role.principals.create({
      principalType: RoleMapping.USER,
      principalId: '56841ad0fb7fdc2004836956'
    }, function(err, principal) {
      if (err) throw err;
      console.log('Created principal:', principal);
    });
  });
};
